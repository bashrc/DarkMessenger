<h1 align="center">Dark Messenger</h1>

<img src="https://code.freedombone.net/bashrc/DarkMessenger/raw/master/art/logo.png?raw=true" width="20%"/>

<p align="center">Private chat, powered by Tor</p>

## Design principles

* Based upon the [Conversations app](https://conversations.im).
* Requires [Orbot](https://guardianproject.info/apps/orbot)
* Uses onion addresses
* Assumes you or someone you trust runs an XMPP server on an onion address
* Don't allow clearnet domains
* TLS not required
* Protect metadata in addition to message content
* OMEMO always enabled
* Dark theme!
* Secure by default and difficult to accidentally use insecurely
* Can be used in parallel with an existing Conversations app installed on a phone. It won't overwrite Conversations or interfere with it.

## Usage

### Sending and receiving attachments

You'll see a self-signed certificate warning when you first try to send or receive an attachment. Select 'always' twice.

### Multi-user chat

When creating a group chat make sure that you have first confirmed two way messaging individually with each of the participants.

### Setting up an XMPP server

If you don't want to configure an XMPP server youself then [Freedombone](https://freedombone.net) provides an easy way to install one which has all the correct XEPs enabled.
